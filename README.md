<img alt="HEX" src="artwork/hex-logo.png"/>

HEX
===

(c) Markus Hoffmann 1997-2021

a simple hext dump utility. 


Description
-----------

Hex is a very simple tool which can process one input file and dump a
hex listing to stdout. 
The output format is fixed: 16 bytes per line.

<pre>
00002000: 01 00 02 00 00 00 00 00 2a 2a 2a 2a 2a 2a 2a 2a  ........********
00002010: 2a 2a 2a 2a 2a 2a 2a 2a 2a 2a 2a 2a 2a 2a 2a 2a  ****************
00002020: 2a 2a 2a 2a 2a 2a 2a 2a 2a 0a 2a 20 20 20 20 20  *********.*     
00002030: 68 65 78 20 20 20 20 20 56 2e 31 2e 30 30 20 20  hex     V.1.00  
00002040: 20 20 20 20 20 20 20 20 20 20 2a 0a 2a 20 28 63            *.* (c
</pre>


### Important Note:

    HEX is free software and comes with NO WARRANTY - read the file
    COPYING for details
    
    (Basically that means, free, open source, use and modify as you like, don't
    incorporate it into non-free software, no warranty of any sort, don't blame me
    if it doesn't work.)
    
    Please read the file INSTALL for compiling instructions.

