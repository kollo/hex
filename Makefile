# Makefile for hex (c) Markus Hoffmann V.1.00

# This file is part of HEX, the hex dump utility 
# ==================================================
# HEX is free software and comes with NO WARRANTY - 
# read the file COPYING for details.

# Insert the defs for your machine

SHELL=/bin/sh

LIBNO=1.00
RELEASE=2
NAME=hex

# Directories
prefix=/usr
exec_prefix=${prefix}
datarootdir=${prefix}/share

BINDIR=${exec_prefix}/bin
DATADIR=${datarootdir}
MANDIR=${datarootdir}/man

# Register variables (-ffixed-reg)
REGS= -fomit-frame-pointer
# Optimization and debugging options
OPT=-O3

# Additional header file paths
INC= -I.

# Compiler
CC=gcc $(REGS)

# Cross-Compiler fuer Windows-Excecutable
WINCC=i686-w64-mingw32-gcc

# Cross-Compiler fuer ARM-Linux-Excecutable
ARMCC=arm-linux-gcc

# Preprocessor
CPP=gcc -E

# Register variables (-ffixed-reg) -Wall
REGS=-fno-omit-frame-pointer  

# Optimization and debugging options
OPT=-O3

# Name of the excecutable
EXE=	$(NAME)
WINEXE= $(NAME).exe

# Headerfiles which should be added to the distribution
HSRC=
CSRC=	hex.c
MAN=	hex.1
DOC=	README.md
DEBDOC= copyright changelog.Debian $(DOC)
WINDOC= readme.txt

DIST=	$(HSRC) $(CSRC) $(DOC) $(MAN) Makefile LICENSE 
BINDIST= LICENSE $(MAN) $(DOC) $(EXE)
WINDIST= $(WINEXE) $(WINDOC)

all:	$(EXE)

$(EXE): $(CSRC) $(HSRC) Makefile 
	$(CC) $(OPT) -o $@ $(CSRC)

# Make the exe for MS WINDOWS
$(WINEXE): $(CSRC) $(HSRC) Makefile 
	$(WINCC) -DWINDOWS $(OPT) $(WINLINKFLAGS) -o $@ $(CSRC) $(WINLIBS)
	strip $@

install: $(EXE) $(MAN)
	install -d $(BINDIR)
	install -s -m 755 $(EXE) $(BINDIR)/
	install -d $(MANDIR)
	install -d $(MANDIR)/man1
	install -m 644 $(NAME).1 $(MANDIR)/man1/
uninstall :
	rm -f $(BINDIR)/$(NAME)
	rm -f $(MANDIR)/man1/$(NAME).1
	rmdir --ignore-fail-on-non-empty $(BINDIR)
	rmdir --ignore-fail-on-non-empty $(MANDIR)/man1
	rmdir --ignore-fail-on-non-empty $(MANDIR)

doc-pak: $(DEBDOC)
	mkdir -p $@
	cp $+ $@/
	gzip -n -9 $@/changelog.Debian

# Make a tar ball with the sources  

dist :	$(NAME)-$(LIBNO).tar.gz
$(NAME)-$(LIBNO).tar.gz : $(DIST)
	rm -rf /tmp/$(NAME)-$(LIBNO)
	mkdir /tmp/$(NAME)-$(LIBNO)
	(tar cf - $(DIST))|(cd /tmp/$(NAME)-$(LIBNO); tar xpf -)
	(cd /tmp; tar cf - $(NAME)-$(LIBNO)|gzip -9 > $@)
	mv /tmp/$@ .

# Make a simple debian package which can easily be installed and removed 
# from the system

deb :	$(BINDIST) doc-pak
	sudo checkinstall -D --pkgname $(NAME) --pkgversion $(LIBNO) \
	--pkgrelease $(RELEASE)  \
	--maintainer kollo@users.sourceforge.net \
	--requires libc6 --backup \
	--pkggroup interpreters  \
	--pkglicense GPL --strip=yes --stripso=yes --reset-uids
	rm -f backup-*.tgz
	sudo chown 1000 $(NAME)_$(LIBNO)-$(RELEASE)_*.deb

windows: $(WINDIST) $(NAME).iss
	rm -f $(NAME)-$(LIBNO)-$(RELEASE)-win.zip
	zip -j -D  -o $(NAME)-$(LIBNO)-$(RELEASE)-win.zip $(WINDIST)
	iscc $(NAME).iss

clean:
	rm -f *.o a.out Makefile.bak backup-*.tgz *.d
distclean: clean
	rm -f $(EXE) $(WINEXE)
	rm -rf doc-pak


